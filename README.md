A passionate FOSS developer involved in many projects, as well as a few commercial projects I work on to make a living.

Note that most of my work is on GitLab, and just mirrored to GitHub, except for projects I contribute to that are on GitHub. Here are my GitHub stats.

<a href="https://github.com/anuraghazra/github-readme-stats">
  <img src="https://github-readme-stats.vercel.app/api?username=hamishmb&count_private=true&show_icons=true" />
</a>

You can contact me at <a href="https://www.hamishmb.com/blog/contact-me/" target="_blank" rel="noopener">https://www.hamishmb.com/blog/contact-me/</a>.
